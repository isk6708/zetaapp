<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\ReferenceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/selamat-datang', function () {
    return view('welcome');
});
Route::get('/senarai-cuti', [LeaveController::class,'SenaraiCuti']);
Route::get('/borang-cuti/{id?}', [LeaveController::class,'MohonCuti']);
Route::post('/simpan-cuti/{id?}', [LeaveController::class,'SimpanCuti']);
Route::post('/batal-cuti', [LeaveController::class,'BatalCuti']);
Route::post('/status-cuti/{jenis_cuti}/{status?}', [LeaveController::class,'StatusCuti']);
Route::delete('/hapus-cuti/{id}', [LeaveController::class,'HapusCuti']);

Route::resource('references', ReferenceController::class);
